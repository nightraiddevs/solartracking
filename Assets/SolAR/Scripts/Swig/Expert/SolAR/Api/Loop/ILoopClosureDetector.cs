//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace SolAR.Api.Loop {

    using XPCF.Api;
    using SolAR.Core;
    using SolAR.Datastructure;
    using SolAR.Api.Solver.Map;

public class ILoopClosureDetector : IComponentIntrospect {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  internal ILoopClosureDetector(global::System.IntPtr cPtr, bool cMemoryOwn) : base(solar_api_loopPINVOKE.ILoopClosureDetector_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(ILoopClosureDetector obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  protected override void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          solar_api_loopPINVOKE.delete_ILoopClosureDetector(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      base.Dispose(disposing);
    }
  }

  public virtual void setCameraParameters(Matrix3x3f intrinsicParams, Vector5f distortionParams) {
    solar_api_loopPINVOKE.ILoopClosureDetector_setCameraParameters(swigCPtr, Matrix3x3f.getCPtr(intrinsicParams), Vector5f.getCPtr(distortionParams));
    if (solar_api_loopPINVOKE.SWIGPendingException.Pending) throw solar_api_loopPINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual FrameworkReturnCode detect(Keyframe queryKeyframe, Keyframe detectedLoopKeyframe, Transform3Df sim3Transform, UIntPairVector duplicatedPointsIndices) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_loopPINVOKE.ILoopClosureDetector_detect(swigCPtr, Keyframe.getCPtr(queryKeyframe), Keyframe.getCPtr(detectedLoopKeyframe), Transform3Df.getCPtr(sim3Transform), UIntPairVector.getCPtr(duplicatedPointsIndices));
    if (solar_api_loopPINVOKE.SWIGPendingException.Pending) throw solar_api_loopPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
