﻿using Google.XR.Cardboard;
using System.Collections;
using UnityEngine;
using UnityEngine.Android;

public class CardboardSolARInit : MonoBehaviour
{
    [SerializeField]
    private SolARTrackingPipeline pipeline;

    private bool isInit = false;

#if UNITY_ANDROID && !UNITY_EDITOR
    public void Update()
    {
        if (!isInit)
        {
            if (pipeline.isUpdateReady && Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
                Screen.brightness = 1.0f;
                if (!Api.HasDeviceParams())
                {
                    Api.ScanDeviceParams();
                }

                isInit = true;
            }

            return;
        }

        if (Api.IsGearButtonPressed)
        {
            Api.ScanDeviceParams();
        }

        if (Api.IsCloseButtonPressed)
        {
            Application.Quit();
        }

        if (Api.IsTriggerHeldPressed)
        {
            Api.Recenter();
        }

        if (Api.HasNewDeviceParams())
        {
            Api.ReloadDeviceParams();
        }

        Api.UpdateScreenParams();
    }
#endif
}
