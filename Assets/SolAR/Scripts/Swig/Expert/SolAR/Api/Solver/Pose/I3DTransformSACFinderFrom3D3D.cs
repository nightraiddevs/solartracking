//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace SolAR.Api.Solver.Pose {

    using XPCF.Api;
    using SolAR.Core;
    using SolAR.Datastructure;
    using SolAR.Api.Input.Files;

public class I3DTransformSACFinderFrom3D3D : IComponentIntrospect {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  internal I3DTransformSACFinderFrom3D3D(global::System.IntPtr cPtr, bool cMemoryOwn) : base(solar_api_solver_posePINVOKE.I3DTransformSACFinderFrom3D3D_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(I3DTransformSACFinderFrom3D3D obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  protected override void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          solar_api_solver_posePINVOKE.delete_I3DTransformSACFinderFrom3D3D(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      base.Dispose(disposing);
    }
  }

  public virtual void setCameraParameters(Matrix3x3f intrinsicParams, Vector5f distortionParams) {
    solar_api_solver_posePINVOKE.I3DTransformSACFinderFrom3D3D_setCameraParameters(swigCPtr, Matrix3x3f.getCPtr(intrinsicParams), Vector5f.getCPtr(distortionParams));
    if (solar_api_solver_posePINVOKE.SWIGPendingException.Pending) throw solar_api_solver_posePINVOKE.SWIGPendingException.Retrieve();
  }

  public virtual FrameworkReturnCode estimate(Point3DfArray firstPoints3D, Point3DfArray secondPoints3D, Transform3Df pose, IntVector inliers) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_solver_posePINVOKE.I3DTransformSACFinderFrom3D3D_estimate__SWIG_0(swigCPtr, Point3DfArray.getCPtr(firstPoints3D), Point3DfArray.getCPtr(secondPoints3D), Transform3Df.getCPtr(pose), IntVector.getCPtr(inliers));
    if (solar_api_solver_posePINVOKE.SWIGPendingException.Pending) throw solar_api_solver_posePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode estimate(Keyframe firstKeyframe, Keyframe secondKeyframe, DescriptorMatchVector matches, Point3DfArray firstPoints3D, Point3DfArray secondPoints3D, Transform3Df pose, IntVector inliers) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_solver_posePINVOKE.I3DTransformSACFinderFrom3D3D_estimate__SWIG_1(swigCPtr, Keyframe.getCPtr(firstKeyframe), Keyframe.getCPtr(secondKeyframe), DescriptorMatchVector.getCPtr(matches), Point3DfArray.getCPtr(firstPoints3D), Point3DfArray.getCPtr(secondPoints3D), Transform3Df.getCPtr(pose), IntVector.getCPtr(inliers));
    if (solar_api_solver_posePINVOKE.SWIGPendingException.Pending) throw solar_api_solver_posePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
