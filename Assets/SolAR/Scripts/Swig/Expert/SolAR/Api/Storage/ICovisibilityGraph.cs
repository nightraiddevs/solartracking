//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace SolAR.Api.Storage {

    using XPCF.Api;
    using SolAR.Core;
    using SolAR.Datastructure;

public class ICovisibilityGraph : IComponentIntrospect {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  internal ICovisibilityGraph(global::System.IntPtr cPtr, bool cMemoryOwn) : base(solar_api_storagePINVOKE.ICovisibilityGraph_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(ICovisibilityGraph obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  protected override void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          solar_api_storagePINVOKE.delete_ICovisibilityGraph(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      base.Dispose(disposing);
    }
  }

  public virtual FrameworkReturnCode increaseEdge(uint node1_id, uint node2_id, float weight) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_increaseEdge(swigCPtr, node1_id, node2_id, weight);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode decreaseEdge(uint node1_id, uint node2_id, float weight) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_decreaseEdge(swigCPtr, node1_id, node2_id, weight);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode removeEdge(uint node1_id, uint node2_id) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_removeEdge(swigCPtr, node1_id, node2_id);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode getEdge(uint node1_id, uint node2_id, out float weight) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_getEdge(swigCPtr, node1_id, node2_id, out weight);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual bool isEdge(uint node1_id, uint node2_id) {
    bool ret = solar_api_storagePINVOKE.ICovisibilityGraph_isEdge(swigCPtr, node1_id, node2_id);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode getAllNodes(UIntSet nodes_id) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_getAllNodes(swigCPtr, UIntSet.getCPtr(nodes_id));
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode suppressNode(uint node_id) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_suppressNode(swigCPtr, node_id);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode getNeighbors(uint node_id, float minWeight, UIntVector neighbors) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_getNeighbors(swigCPtr, node_id, minWeight, UIntVector.getCPtr(neighbors));
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode minimalSpanningTree(IIFTupleVector edges_weights, out float minTotalWeights) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_minimalSpanningTree(swigCPtr, IIFTupleVector.getCPtr(edges_weights), out minTotalWeights);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode maximalSpanningTree(IIFTupleVector edges_weights, out float maxTotalWeights) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_maximalSpanningTree(swigCPtr, IIFTupleVector.getCPtr(edges_weights), out maxTotalWeights);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode getShortestPath(uint node1_id, uint node2_id, UIntVector path) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_getShortestPath(swigCPtr, node1_id, node2_id, UIntVector.getCPtr(path));
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode display() {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_display(swigCPtr);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode saveToFile(string file) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_saveToFile(swigCPtr, file);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public virtual FrameworkReturnCode loadFromFile(string file) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_storagePINVOKE.ICovisibilityGraph_loadFromFile(swigCPtr, file);
    if (solar_api_storagePINVOKE.SWIGPendingException.Pending) throw solar_api_storagePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
