//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 4.0.2
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace SolAR.Api.Solver.Pose {

    using XPCF.Api;
    using SolAR.Core;
    using SolAR.Datastructure;
    using SolAR.Api.Input.Files;

public class I2D3DCorrespondencesFinder : IComponentIntrospect {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  internal I2D3DCorrespondencesFinder(global::System.IntPtr cPtr, bool cMemoryOwn) : base(solar_api_solver_posePINVOKE.I2D3DCorrespondencesFinder_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(I2D3DCorrespondencesFinder obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  protected override void Dispose(bool disposing) {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          solar_api_solver_posePINVOKE.delete_I2D3DCorrespondencesFinder(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      base.Dispose(disposing);
    }
  }

  public virtual FrameworkReturnCode find(Frame lastFrame, Frame currentFrame, DescriptorMatchVector current_matches, Point3DfArray shared_3dpoint, Point2DfArray shared_2dpoint, SWIGTYPE_p_std__vectorT_std__pairT_unsigned_int_boost__shared_ptrT_SolAR__datastructure__CloudPoint_t_t_t corres2D3D, DescriptorMatchVector found_matches, DescriptorMatchVector remaining_matches) {
    FrameworkReturnCode ret = (FrameworkReturnCode)solar_api_solver_posePINVOKE.I2D3DCorrespondencesFinder_find(swigCPtr, Frame.getCPtr(lastFrame), Frame.getCPtr(currentFrame), DescriptorMatchVector.getCPtr(current_matches), Point3DfArray.getCPtr(shared_3dpoint), Point2DfArray.getCPtr(shared_2dpoint), SWIGTYPE_p_std__vectorT_std__pairT_unsigned_int_boost__shared_ptrT_SolAR__datastructure__CloudPoint_t_t_t.getCPtr(corres2D3D), DescriptorMatchVector.getCPtr(found_matches), DescriptorMatchVector.getCPtr(remaining_matches));
    if (solar_api_solver_posePINVOKE.SWIGPendingException.Pending) throw solar_api_solver_posePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
