Combined Google Cardboard SDK and SolAR Framework
# SolarTracking
To install the Solar Tracking package (SolarTracking.unitypackage) to another project, do the following:
# Cardboard SDK Installation
Install Cardboard SDK according to this tutorial:
https://developers.google.com/cardboard/develop/unity/quickstart
(If you need tracking only without the cardboard functionality, this step can be omitted, but the "Cardboard" demo scene in the package won't work)
# SolAR Framework Installation
Import package downloaded from here:
https://github.com/SolarFramework/SolARUnityPlugin/releases/tag/0.9.0
Make Unity setup according to this tutorial:
https://solarframework.github.io/install/android/#unity_solar_android
(You don't need to execute .bat files)
# SolAR framework installation
Since this package depends on the abovementioned libraries, you should install it last.
Just import the SolarTracking.unitypackage and try the demo scenes.
