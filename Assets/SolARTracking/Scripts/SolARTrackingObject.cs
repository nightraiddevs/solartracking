﻿using SolAR;
using UnityEngine;
using UnityEngine.Assertions;

namespace SolARTracking
{
    public class SolARTrackingObject : MonoBehaviour
    {
        [SerializeField]
        protected AbstractSolARPipeline solARPipeline;
        [SerializeField]
        private Transform rotationControlTransform;
        [SerializeField]
        private Transform nextTransform;
        [SerializeField]
        private Transform trackedTransform;

        [SerializeField]
        private float cameraXShift;
        [SerializeField]
        private float distanceLerpSpeed;
        [SerializeField]
        private float rotationLerpSpeed;
        [SerializeField]
        private float rotationThresholdDegrees;
        [SerializeField]
        private float positionThresholdUnits;

        private float distanceLerpTime = 0;
        private Vector3 currentPosition = Vector3.zero;
        private Vector3 nextPosition = Vector3.zero;

        private float rotationLerpTime = 0;
        private Quaternion currentRotation = Quaternion.identity;
        private Quaternion nextRotation = Quaternion.identity;

        protected void OnEnable()
        {
            Assert.IsNotNull(solARPipeline);
            Assert.IsNotNull(rotationControlTransform);
            Assert.IsNotNull(nextTransform);
            solARPipeline.OnStatus += OnStatus;
        }

        protected void OnDisable()
        {
            solARPipeline.OnStatus -= OnStatus;
        }

        void OnStatus(bool isTracking)
        {
            if (!isTracking)
            {
                return;
            }

            var pose = solARPipeline.Pose;
            SetPose(pose);
        }

        private void SetPose(Pose pose)
        {
            nextTransform.localPosition = -pose.position;
            rotationControlTransform.localRotation = pose.rotation;
            var prevParent = nextTransform.parent;
            nextTransform.parent = rotationControlTransform;
            rotationControlTransform.localRotation = Quaternion.identity;
            nextTransform.parent = prevParent;


            nextTransform.localRotation = Quaternion.Inverse(pose.rotation);

            if (Screen.orientation == ScreenOrientation.LandscapeRight)
            {
                nextTransform.localPosition -= new Vector3(cameraXShift, 0, 0);
            }
            else
            {
                nextTransform.localPosition += new Vector3(cameraXShift, 0, 0);
            }

            SetRotation();
            SetPosition();
        }

        private void SetRotation()
        {
            var rotDist = Quaternion.Angle(trackedTransform.rotation, nextTransform.rotation);
            float rotLerpStatus = (Time.time - rotationLerpTime) * rotationLerpSpeed;
            if (rotDist >= rotationThresholdDegrees && rotLerpStatus >= 1)
            {
                rotationLerpTime = Time.time;
                currentRotation = trackedTransform.rotation;
                nextRotation = nextTransform.rotation;
            }

            rotLerpStatus = (Time.time - rotationLerpTime) * rotationLerpSpeed;
            trackedTransform.rotation = Quaternion.Lerp(currentRotation, nextRotation, rotLerpStatus);
        }

        private void SetPosition()
        {
            var posDist = Vector3.Distance(trackedTransform.position, nextTransform.position);
            float distLerpStatus = (Time.time - distanceLerpTime) * distanceLerpSpeed;
            if (posDist >= positionThresholdUnits && distLerpStatus >= 1)
            {
                distanceLerpTime = Time.time;
                currentPosition = trackedTransform.position;
                nextPosition = nextTransform.position;
            }

            distLerpStatus = (Time.time - distanceLerpTime) * distanceLerpSpeed;
            trackedTransform.position = Vector3.Lerp(currentPosition, nextPosition, distLerpStatus);
        }
    }
}